public class Temperature {
    final private double value;
    final private char nameOfScale;

    public Temperature(double value){
        this.value = value;
        this.nameOfScale = 'c';
    }

    private Temperature(double value, char nameOfScale){
        this.value = value;
        this.nameOfScale = nameOfScale;
    }

    public Temperature convertToKelvinFromCelsius (){
        if(nameOfScale == 'c')
             return new Temperature(value + 273.15, 'k');
        else throw new ConvertException("This temperature is not Celsius");
    }

    public Temperature convertToCelsiusFromKelvin (){
        if(nameOfScale == 'k')
            return new Temperature(value - 273.15, 'c');
        else throw new ConvertException("This temperature is not Kelvin");
    }

    public Temperature convertToFahrenheitFromCelsius (){
        if(nameOfScale == 'c')
            return new Temperature(value*9/5 + 32, 'f');
        else throw new ConvertException("This temperature is not Celsius");
    }

    public Temperature convertToCelsiusFromFahrenheit (){
        if(nameOfScale == 'f')
            return new Temperature(5*(value-32)/9, 'c');
        else throw new ConvertException("This temperature is not Fahrenheit");
    }

    public Temperature convertToKelvinFromFahrenheit(){
        if (nameOfScale == 'f')
            return new Temperature(convertToCelsiusFromFahrenheit().convertToKelvinFromCelsius().value);
        else throw new ConvertException("This temperature is not Fahrenheit");
    }

    public Temperature convertToFahrenheitFromKelvin(){
        if (nameOfScale == 'k')
            return new Temperature(convertToCelsiusFromKelvin().convertToFahrenheitFromCelsius().value);
        else throw new ConvertException("This temperature is not Kelvin");
    }

    public int compare(Temperature temperature){
        if(this.nameOfScale!=temperature.nameOfScale)
           throw new ConvertException("Temperature scales don't match");

        return Double.compare(this.value, temperature.value);
    }

    public Temperature subtraction(Temperature temperature){
        if(this.nameOfScale!=temperature.nameOfScale)
            throw new ConvertException("Temperature scales don't match");

        return new Temperature(this.value - temperature.value, this.nameOfScale);
    }
}
